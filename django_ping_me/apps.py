from django.apps import AppConfig


class PingMeConfig(AppConfig):
    name = 'ping_me'
